class WelcomeController < ApplicationController

  def welcome
    prng = Random.new
    prng = prng.rand(100)       # => 42
    if prng > 50
      @organisations =  [ { "created_at": "2019-01-24T22:08:44.871+00:00", "created_by_user_id": "7BFIwJCPaahJP7", "id": "7CE8TPImGkkumP", "name": "dsa", "updated_at": "2019-01-24T22:08:44.871+00:00", "email": "" }, { "created_at": "2019-02-12T19:33:10.588+00:00", "created_by_user_id": "7BFIwJCPaahJP7", "email": "", "id": "7GmfdLgXYCEWDJ", "name": "lkjhgfdsa", "updated_at": "2019-02-12T19:33:10.588+00:00" } ]
    else
      @organisations =  [ { "created_at": "2019-02-12T19:33:10.588+00:00", "created_by_user_id": "7BFIwJCPaahJP7", "email": "", "id": "7GmfdLgXYCEWDJ", "name": "lkjhgfdsa", "updated_at": "2019-02-12T19:33:10.588+00:00" } ]
    end
    respond_to do |format|
      format.xml { render(xml:   @organisations) }
      format.json { render(json:   @organisations ) }
    end
  end
end
